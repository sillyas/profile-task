/*Tabs*/

document.body.addEventListener('click', function (e) {
  let currentElement = e.srcElement,
    tabToggleClass = 'tab-toggle',
    modalToggleClass = 'modal-toggle',
    modalCloseClass = 'modal-close';

  if(currentElement.classList.contains(tabToggleClass)) {
    tabsProcessing(currentElement, tabToggleClass);
  } else if(currentElement.classList.contains(modalToggleClass)) {
    openModal(currentElement);
  } else if(currentElement.classList.contains(modalCloseClass)) {
    closeModal(currentElement);
  }
});

function tabsProcessing(currentElement, toggleClass) {
    let currentToggle = currentElement,
        tabClass = 'tab',
        controls = currentToggle.attributes['aria-controls'];

    if(controls) {
        let currentTab = document.getElementById(controls.value);

        if(currentTab) {
            let allTabs = document.getElementsByClassName(tabClass),
                allToggles = document.getElementsByClassName(toggleClass);

            [...allTabs].forEach(function (tab) {
                tab.classList.remove('show');
            });

            [...allToggles].forEach(function (toggle) {
                toggle.setAttribute('aria-expanded','false');
            });

            currentToggle.setAttribute('aria-expanded','true');
            currentTab.classList.add('show');
        }
    }
}

function openModal(currentElement) {
    let currentToggle = currentElement,
        modalClass = 'modal-wrapper',
        controls = currentToggle.dataset['target'];

    if(controls) {
        let currentModal = document.getElementById(controls);

        if(currentModal) {
            if(modalPasteImage(currentElement, currentModal)) {
                let allModals = document.getElementsByClassName(modalClass);

                [...allModals].forEach(function (modal) {
                    modal.setAttribute('aria-hidden','true');
                });

                document.body.classList.add('fixed');
                currentModal.setAttribute('aria-hidden','false');
            }
        }
    }
}

function closeModal(currentElement) {
    let modalClass = 'modal-wrapper',
        currentModal = closestParent(currentElement, modalClass);

    document.body.classList.remove('fixed');
    currentModal.setAttribute('aria-hidden','true');
}

function modalPasteImage(currentElement, currentModal) {
    let currentImage = currentElement.children[0];

    if(currentImage && currentImage.nodeName === 'IMG') {
        let currentImageSource = currentImage.attributes['src'];

        if(currentImageSource) {
            let modalImage = currentModal.querySelector('.modal-image'),
                modalImageSource = (currentImageSource.value),
                imageSourceOrigin = modalImageSource.replace('-thumb','');

            modalImage.setAttribute('src',imageSourceOrigin);

            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function closestParent(el, cls) {
    while ((el = el.parentElement) && !el.classList.contains(cls));
    return el;
}